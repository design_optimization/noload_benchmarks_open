#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

import math
import autograd.numpy as np

"""
solarPV.py: PV Production model based on solar position depending on the time 
of day and the day of the year and irradiance projection on the PV panel 
depending on its orientation
"""

__author__ = "B.Delinchant / G2ELab"


class PVSolar:
    '''
    PVSolar Class constructor.
    Keyword arguments:
        timestamp -- the time serie of data (required)
        latitude -- latitude of the solar panel (default 45.2 for Grenoble,
        FRANCE)
        longitude -- longitude of the solar panel (default -5.7 for Grenoble,
        FRANCE)
    '''

    def __init__(self, timestamp: "panda series", latitude=45.2,
                 longitude=-5.7):
        self.year = timestamp
        self.L = longitude  # Longitude (in degree): °E negative, °O positive
        self.lat = latitude  # Latitude : Nord : positive
        self.DOY = np.array(timestamp.dt.dayofyear,
                            dtype=float)  # Day of the Year
        self.dim_vec = len(timestamp)
        self.UT_Hour = np.array(
            (timestamp.dt.hour + timestamp.dt.minute / 60).values, dtype=float)
        # Taking into account the time difference must already be integrated
        # into the data. The radiation is assumed to be GMT / UTH
        # tau = np.ones(self.dim_vec)  # 1 in winter
        # tau[(self.DOY > 89) & (self.DOY < 299)] = tau[(self.DOY > 89) &
        # (self.DOY < 299)] + 1  # 2 in summer
        # self.UT_Hour = self.UT_Hour - tau
        # self.UT_Hour[self.UT_Hour < 0] = self.UT_Hour[self.UT_Hour < 0] + 24

        PHI = self.lat * math.pi / 180

        B = 2 * math.pi * (self.DOY - 81) / 365.25  # Sun's ecliptic longitude
        # (shifted by 90°)
        ET = 9.87 * np.sin(2 * B) - 7.53 * np.cos(B) - 1.5 * np.sin(B)
        delta = math.pi / 180 * 23.45 * np.sin(B)  # Declination angle
        TSV = self.UT_Hour - self.L / 15 + ET / 60  # TSV=Solar Time;
        # 5.7245=Longitude of standard meridian-Longgitude  of Grenoble
        w = 15 * (TSV - 12) * math.pi / 180  # Hour angle
        self.cosW = np.cos(w)
        self.sinW = np.sin(w)
        self.sinDelta = np.sin(delta)
        self.cosDelta = np.cos(delta)
        self.sinPHI = np.sin(PHI)
        self.cosPHI = np.cos(PHI)
        # self.ts = timestamp

    def pv_power(self, tilt, azimuth, surface, DNI, DHI):
        """
        Computes PV production array (W)
        Keyword arguments:
            tilt -- vertical angle of panel plane (degrees). Horizontal panel
            correspond to 0 (required)
            azimuth -- horizontal angle of panel plane (degrees). South panel
            correspond to 0 (required)
            surface -- surface of the panel (m²)
            DNI -- Direct Normal Irradiance (W/m²)
            DHI -- Diffuse Horizontal Irradiance (W/m²)

            return PV production (kW.h)
        """
        n_PV = 0.15  # efficiency
        rho = 0.2  # Ground albedo
        beta = tilt * math.pi / 180  # in radian
        gamma = azimuth * math.pi / 180  # in radian

        cosGamma = np.cos(gamma)
        sinGamma = np.sin(gamma)

        cosBeta = np.cos(beta)
        sinBeta = np.sin(beta)

        costheta = self.sinDelta * (self.sinPHI * cosBeta - self.cosPHI *
                                    sinBeta * cosGamma) \
                   + self.cosDelta * self.cosW * \
                   (self.cosPHI * cosBeta + self.sinPHI * sinBeta * cosGamma) \
                   + self.cosDelta * sinBeta * sinGamma * self.sinW

        indexes = np.where(costheta < 0)
        condition = np.ones(self.dim_vec)
        condition[indexes] = 0
        IDir_module = DNI * costheta * condition
        DHI_module = DHI * ((1.0 + cosBeta) / 2)
        IRef_module = (DNI + DHI) * rho * ((1.0 - cosBeta) / 2)
        pPV = surface * n_PV * (IDir_module + DHI_module + IRef_module)

        return pPV

    # TODO : time step calculation if different from 1 hour
    def pv_energy(self, tilt, azimuth, surface, DNI, DHI):
        """
        Computes energy production
        Keyword arguments:
            tilt -- vertical angle of panel plane (radian). Horizontal panel
            correspond to 0 (required)
            azimuth -- horizontal angle of panel plane (radian). South panel
            correspond to 0 (required)
            surface -- surface of the panel (m²)
            return PV production (kW.h)
        """
        pPV = self.pv_power(tilt, azimuth, surface, DNI, DHI)
        return np.sum(pPV) * 1e-3
