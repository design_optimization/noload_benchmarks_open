#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

"""
energy.py: energy model for a dwelling PV self-consumption with NoLOAD
"""

__author__ = "B.Delinchant / G2ELab"

try:
    import pv_battery_coverage_gwp.models.solarPV as solarPV
except ModuleNotFoundError:
    import models.solarPV as solarPV
import autograd.numpy as np


def cover_factors(
        tilt: 'tilted angle of panel (degree)',
        azimuth: 'solar azimuth angle (degree)',
        surface: 'panel surface (m²)',
        e_sto: 'storage charge or discharge energy (Wh)',
        irradiance: 'Dataframe of Direct Normal Irradiation (W/m²) and Global'
                    ' Horizontal Irradiation (W/m²)',
        consumption: 'time vector of power consumption (W)', ):
    solar = solarPV.PVSolar(irradiance.index.to_series())
    dni = np.array(irradiance['DNI'])
    dhi = np.array(irradiance['DHI'])
    eff_sto_lead_acid = 0.75  # Charging and discharging storage efficiency
    eff_sto_li_ion = 0.9  # Charging and discharging storage efficiency

    # PV Power
    pv_power = solar.pv_power(tilt, azimuth, surface, dni, dhi)
    # time balance between production and consumption
    e_balance_vect = pv_power - consumption
    # energy imported and exported, depending on hourly balance, and the total
    # battery energy charged/discharged, taking into account the storage
    # efficiency.
    energy_import = -np.sum(e_balance_vect[e_balance_vect < 0]) \
                    - eff_sto_li_ion * e_sto
    energy_export = np.sum(e_balance_vect[e_balance_vect > 0]) \
                    - eff_sto_li_ion * e_sto

    # Used without constraint on export and import
    # energy_import = -np.sum(e_balance_vect[e_balance_vect < 0]) \
    #                 - min(eff_sto_lead_acid * e_sto, -np.sum(e_balance_vect[
    #                                                              e_balance_vect < 0]),
    #                       np.sum(e_balance_vect[e_balance_vect > 0]))
    # energy_export = np.sum(e_balance_vect[e_balance_vect > 0]) \
    #                 - min(eff_sto_lead_acid * e_sto, -np.sum(e_balance_vect[
    #                                                              e_balance_vect < 0]),
    #                       np.sum(e_balance_vect[e_balance_vect > 0]))

    pv_supply = pv_power.sum()  # PV energy
    load = consumption.sum()  # Load energy
    e_balance = np.sum(e_balance_vect)
    coverage = pv_supply - energy_export
    # checking the definition with another one :
    coverage2 = load - energy_import
    assert (abs(coverage - coverage2) < 1e-5)
    # defining the cover factors, between 1 and 0 :
    supply_cover_factor = coverage / pv_supply
    load_cover_factor = coverage / load

    # avoid return vectors if not required, it may increase Jacobian
    # calculation time
    return supply_cover_factor, load_cover_factor, energy_import, \
           energy_export, pv_supply, load, coverage, e_balance


# locals().items()  # don't change it, it  is required for NoLOAD toolbox

# Using AUTOGRAD : Don't use
#     Assignment to arrays A[0,0] = x
#     Implicit casting of lists to arrays A = np.sum([x, y]),
# use A = np.sum(np.array([x, y])) instead.
#     A.dot(B) notation (use np.dot(A, B) instead)
#     In-place operations (such as a += b, use a = a + b instead)
#     Some isinstance checks, like isinstance(x, np.ndarray) or
# isinstance(x, tuple), without first doing from autograd.builtins
# import isinstance, tuple.
