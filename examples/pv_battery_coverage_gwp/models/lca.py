#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

# depending on where the code is run: python file or jupyter notebook
try:
    from pv_battery_coverage_gwp.data.lca_data import *
except ModuleNotFoundError:
    from data.lca_data import *

"""
Global warming potential [kgCO2]
Functional unit : producing electricity from 1m² PV, and 1kWh storage
during 30 years
Keyword arguments:
    surface -- PV surface (m²), 1kWp = 6.67m²
    batteryCapacity -- energy stored in the battery (Wh)
    gridImport -- energy imported from the grid (kWh)
"""


def gwp_calc(surface, batteryCapacity, gridImport):
    PV_GWP = CO2_PV * COEF_PV  # per m²
    BATTERY_GWP = CO2_BAT * COEF_BAT  # per Wh

    gwp = PV_GWP * surface + BATTERY_GWP * batteryCapacity \
          + MIX_EUROPE_GWP * STUDY_TIME * gridImport

    return gwp
