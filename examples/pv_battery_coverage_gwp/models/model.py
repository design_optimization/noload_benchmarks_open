#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

__author__ = "B.Delinchant / G2ELab"
# depending on where the code is run: python file or jupyter notebook
try:
    import examples.pv_battery_coverage_gwp.models.LCA as lca
except ModuleNotFoundError:
    import models.LCA as lca


def objectives_and_constraints(
        tilt: 'tilted angle of panel (degree)',
        azimuth: 'solar azimuth angle (degree)',
        surface: 'panel surface (m²)',
        e_sto_1, e_sto_2, e_sto_3: 'Storage charge - discharge energy for '
                                   'each typical day (Wh)',
        irradiance_list: 'Diffuse Horizontal and Direct Normal Irradiations '
                         '(W/m²)',
        consumption_list: 'vectors of power consumption (W)',
        typical_day_nb: 'Typical day number',
        typical_day_weight: 'Typical day weight in the year', ):
    # Cover factors
    try:
        import pv_battery_coverage_gwp.models.energy as nrj
    except ModuleNotFoundError:
        import models.energy as nrj

    # list of variables from the various typical days
    scf_list = [None] * typical_day_nb
    lcf_list = [None] * typical_day_nb
    import_list = [None] * typical_day_nb
    export_list = [None] * typical_day_nb
    supply_list = [None] * typical_day_nb
    load_list = [None] * typical_day_nb
    coverage_list = [None] * typical_day_nb
    e_balance_list = [None] * typical_day_nb

    e_sto_list = [e_sto_1, e_sto_2, e_sto_3]

    for i in range(0, typical_day_nb):
        scf_list[i], lcf_list[i], import_list[i], export_list[i], \
        supply_list[i], load_list[i], coverage_list[i], e_balance_list[i] = \
            nrj.cover_factors(tilt, azimuth, surface, e_sto_list[i],
                              irradiance_list[i], consumption_list[i])

    # Test en absence de vectoriel
    export_0 = export_list[0]
    export_1 = export_list[1]
    export_2 = export_list[2]
    import_0 = import_list[0]
    import_1 = import_list[1]
    import_2 = import_list[2]

    non_coverage = 0
    grid_import = 0
    grid_export = 0
    non_scf = 0
    non_lcf = 0
    for d in range(0, typical_day_nb):
        # General non-coverage objective indicator, taking into account load
        # cover factors and supply cover factors of each typical day.
        non_coverage += typical_day_weight[d] / 365 * \
                        ((1 - scf_list[d]) + (1 - lcf_list[d])) / 2

        # Objectives for scf or lcf alone
        non_scf += typical_day_weight[d] / 365 * (1 - scf_list[d])
        non_lcf += typical_day_weight[d] / 365 * (1 - lcf_list[d])

        # Annual energy estimation
        grid_import += typical_day_weight[d] * (import_list[d]) * 0.001  # kWh
        grid_export += typical_day_weight[d] * (export_list[d]) * 0.001  # kWh

    # Battery capacity calculation
    dod_lead_acid = 0.5  # Depth of Discharge
    dod_li_ion = 0.8
    max_e_sto = max(e_sto_list)
    capa_bat = max_e_sto / dod_lead_acid  # Taking into account the state
    # of charge

    # Global Warming Potential calculation

    gwp = lca.gwp_calc(surface, capa_bat, grid_import)  # m², Wh,
    # kWh per year

    # biobjectif = non_coverage + gwp / 2000
    return locals().items()  # don't change it, it  is required for NoLOAD
    # toolbox
