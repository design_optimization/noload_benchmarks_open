import requests
import pandas as pd
import datetime
from datetime import datetime, timedelta

__author__ = "Salman Shahid"
__copyright__ = "Copyright 2019, The Expesigno project"
__credits__ = ["Expesigno"]
__licence__ = "Apache 2.0"
__version__ = "0.1"
__maintainer__ = "Salman Shahid, Sacha Hodencq"
__email__ = "muhammad-salman.shahid@g2elab.grenoble-inp.fr"
__status__ = "Completed"

"Version obtained from the 1.0 but removing functions that were not used here"

###############################################################################
# MAKING DATAFRAMES WITH NO TIMEZONE INFO
###############################################################################
def dataframe_with_no_tzinfo(dataframe, column_name):
    """
    MAKING DATAFRAMES WITH NO TIMEZONE INFO
    :param dataframe:
    :param column_name:
    :return: dataframe:
    """

    # Sorting dataframe according to given column
    dataframe = dataframe.sort_values(by=column_name)

    # Converting column to DateTImeIndex
    dataframe[column_name] = pd.to_datetime(dataframe[column_name],
                                            errors='coerce')

    # Making the tzinfo =None i.e. removing the timezone info
    dataframe[column_name] = dataframe[column_name].dt.tz_convert('Europe/Paris')
    dataframe[column_name] = dataframe[column_name].dt.tz_localize(None)

    # Setting the column as dataframe index
    dataframe = dataframe.set_index([column_name])

    return dataframe


###############################################################################
#   ACCESSING COEFFICIENT DES PROFILES CURVE
###############################################################################
def coefficient_des_profils(starting_date, nb_days):
    """
    Accessing the coefficient des profiles for nb_days
    :return: dataframe, train_dataframe, val_dataframe, test_dataframe
    """
    # Defining the parameters of the API to be accessed
    dataset = "coefficients-des-profils"
    rows = 48
    categorie = "Residentiel"
    sous_profile = "RES1_BASE"
    format_datetime = "%Y-%m-%d"

    dataframe_starting_date = starting_date + timedelta(days=1)

    # defining a dataframe
    dataframe = pd.DataFrame()

    empty_curves = []

    for number_of_days in range(0, nb_days, 1):
        date = starting_date + timedelta(days=number_of_days)

        # print("Accessing coefficient des profils data for {}".format(
        #     date.strftime(format_datetime)))

        url = "https://data.enedis.fr/api/records/1.0/search/?dataset={}" \
              "&rows={}&facet=horodate&facet=sous_profil&facet=categorie&" \
              "refine.categorie={}&refine.sous_profil={}&refine." \
              "horodate={}%2F{}%2F{}".format(dataset, rows, categorie,
                                             sous_profile, date.year,
                                             date.month, date.day)

        response = requests.get(url)

        if response.status_code == 200:

            data = list(response.json().values())
            curve = data[2]

            if curve:

                # appending in the dataframe
                for i in range(0, len(curve), 1):
                    row = pd.Series([curve[i]['fields']['horodate'],
                                     curve[i]['fields']['coefficient_prepare'],
                                     curve[i]['fields']['coefficient_ajuste'],
                                     curve[i]['fields']
                                     ['coefficient_dynamique']])
                    dataframe = dataframe.append(row, ignore_index=True)

            else:

                empty_curves.append(curve)

        else:
            title = "STATUS: Coefficients des Profils"
            message = "ERROR: No data of coefficient des profils is" \
                      " obtained by ENEDIS. Status Code: {}".format(
                response.status_code)

        if len(empty_curves) == 4:

            if not (empty_curves[0] and empty_curves[1] and empty_curves[2] and empty_curves[3]):
                break

    if not (dataframe.empty):
        column_name = 0

        dataframe = dataframe_with_no_tzinfo(dataframe=dataframe, column_name=column_name)

        # # Sorting dataframe according to given column
        # dataframe = dataframe.sort_values(by=column_name)
        #
        # # Converting column to DateTImeIndex
        # dataframe[column_name] = pd.to_datetime(dataframe[column_name],
        # errors='coerce')
        #
        # # Making the tzinfo =None i.e. removing the timezone info
        # dataframe[column_name] = dataframe[column_name].dt.
        # tz_localize('UTC').dt.tz_convert('Europe/Berlin')
        # dataframe[column_name] = dataframe[column_name].dt.tz_localize(None)
        #
        # # Setting the column as dataframe index
        # dataframe = dataframe.set_index([column_name])
        # dataframe = dataframe.sort_index()

        # Renaming columns
        dataframe.columns = ['COEFFICIENT_PREPARE', 'COEFFICIENT_AJUSTE',
                             'COEFFICIENT_DYNAMIQUE']

        # Taking D+1 as starting date
        dataframe = dataframe[dataframe_starting_date.strftime(
            format_datetime):].astype("float32")

    return dataframe

"""
TEST
"""
if __name__ == "__main__":
    dataframe = coefficient_des_profils(starting_date=datetime(year=2020,
                                                               month=2,
                                                               day=18),
                                        nb_days=2)
    # d = smoothened_national_temperature(starting_date=datetime(year=2020, month=6, day=18))
    enedis_file_name = "enedis_RES1_profile"
    h_df = dataframe.resample('H').mean()
    h_df.to_csv(path_or_buf=enedis_file_name, sep=';')
    print("exported results as {}".format(enedis_file_name))
