# Metadata for NoLOAD notebooks datasets 

## irradiance_and_consumption_data
- The dataset includes data at hourly time step for the year 2019:
   - Direct Normal Irradiation (DNI) & Diffuse Horizontal Irradiation (DHI) in 
   W/m² coming from the the GreEn-Er (G2Elab and ENSE3 school building) 
   [weather station](https://mhi-srv.g2elab.grenoble-inp.fr/grafana/d/VnkHFo6mk/dashboard) 
   - Mean consumption of a French dwelling, based on Enedis (French DSO), 
   [RES1 profile](https://www.enedis.fr/coefficients-des-profils) i.e. consumption profiles for dwellings connected in 
   low-voltage with nominal power lower than 6 kVA. We consider an annual 
   consumption of 4453 kWh per year and per dwelling, when applying an 
   annual consumption of 4586 KWh to the RES1 profiles. This consumption is 
   calculated from the number of households connected to the French power 
   system according to this [French Energy Regulatory Commission (CRE) 
   report](https://www.cre.fr/Documents/Publications/Rapports-thematiques/Etat-des-lieux-des-marches-de-detail-de-l-electricite-et-du-gaz-naturel-en-2017), page 19 (32.4 million connected residential consumers in 2017),
    and the final electricity consumption of the French residential sector 
    of [148.6 TWh in 2019](https://opendata.reseaux-energies.fr/explore/dataset/consommation-annuelle-nette-typologie/table/?disjunctive.segment&sort=annee).
- Format: .csv
- The data were collected and processed by Sacha Hodencq (G2Elab) for an 
article at the BS conference. They are used in the [ORUCE Notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/omegalpes/omegalpes_examples/-/blob/master/notebooks/article_2021_BS_ORUCE.ipynb), and 
their assembling and processing is described in a [NoLOAD notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/design_optimization/noload_benchmarks_open/-/blob/master/examples/pv_battery_coverage_gwp/pv_battery_coverage_gwp.ipynb)
- License: see LICENCE file
- Detail: regarding the DNI & DHI datasets, sensors changed on the 20th of 
February. Data are extrapolated from 11am to 1pm this day, as well as for 
other periods with sensors issue:
   - 21th of february at 11am
   - 3rd of april from 1 to 3pm
   - 17th of december at 9pm

  