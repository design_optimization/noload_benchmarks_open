#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

"""
This module includes lca data
"""

CO2_PV = 1040  # 6,67m² @1kwc, PV module monocrystalline silicon including
# inverter cost
CO2_BAT = 351  # Lead-acid battery [capacity=200Ah], 38.6kg, 3 years
CO2_BAT_LI_ION = 158  # Li-ion battery [capacity=1kWh], 8 years
LIF_PV = 30  # Lifespan of the PV panels (years)
LIF_BAT = 3  # Lifespan of the Lead-Acid batteries (years)
LIF_BAT_LI_ION = 8  # Lifespan of the Li-Ion batteries (years)
STUDY_TIME = 30  # Study time in years

COEF_PV = 1.0 / 6.67 * STUDY_TIME/LIF_PV  # 1kWp => 1 m² , 30 years
COEF_BAT = 1.0 / (200 * 12) * STUDY_TIME/LIF_BAT  # 200Ah, 12V, 3 years
# => 30 years
COEF_BAT_LI_ION = 1.0 / (1000) * STUDY_TIME/LIF_BAT_LI_ION
# => 30 years

MIX_GWP = 0.0571  # kgCO2eq / kWh http://bilans-ges.ademe.fr/,2018
# french electricity mix - consumption mean value
MIX_EUROPE_GWP = 0.250   # kgCO2eq / kWh european electricity mix EEA 2019
