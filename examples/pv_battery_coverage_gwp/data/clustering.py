#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

# Main contributors : Prasaant BALASUNDARAM, Sacha Hodencq

import numpy as np
# import seaborn as sns
import matplotlib.pyplot as plt
from pandas import DataFrame
import pandas as pd
# from sklearn.feature_selection import SelectKBest
# from sklearn.feature_selection import chi2
from scipy.cluster.vq import vq, kmeans, whiten
# import random
from sklearn.metrics import pairwise_distances_argmin_min
from sklearn import preprocessing
# from mpl_toolkits.mplot3d import Axes3D
# import pv_battery_coverage_gwp.data.data_management as dm


def clustering(file_name, centroids_nb, print_results):
    #TODO : quantify distortion of the cluster
    # import data from csv file to data frame
    data = pd.read_csv(file_name, delimiter=';', decimal=".")
    # choose the features to make the clustering
    df = DataFrame(data, columns=['Consumption', 'DHI', 'DNI'])
    df_values = df.values
    # normalize the data in each column since data from different scale
    # are used
    df_pre = preprocessing.minmax_scale(df_values)
    x_plot = np.reshape(df_values, (365, 24, 3))
    x = np.reshape(df_pre, (365, 24, 3))
    whitened = whiten(x)
    # reshape data into required array in this case the vector of 72 values
    # represents one day
    reshaped_whit = whitened.reshape(365, 24 * 3)
    # manually choose the starting centroid if required
    # choice for number of clusters is 7
    # book = np.array((whit[171], whit[348]))

    centroids, distortion = kmeans(reshaped_whit, centroids_nb)  # book)
    # labels
    idx, _ = vq(reshaped_whit, centroids)
    closest, _ = pairwise_distances_argmin_min(centroids, reshaped_whit)

    if print_results:
        # print number of days in each cluster to verify the distribution of
        # the cluster
        print("Number of days in each cluster")
        print(np.bincount(idx))
        # print the indices of the days closes to the cluster center
        print("Indices of days close to the cluster center")
        print(closest)

        # Subplots for clustering data
        time = range(0, 24)
        fig1, axs1 = plt.subplots(3)
        fig1.suptitle('Clustering data sets')
        for cons_day in x_plot[:, :, 0]:
            axs1[0].plot(time, cons_day)
        axs1[0].set_title('Consumption profiles (W)')
        for dhi_day in x_plot[:, :, 1]:
            axs1[1].plot(time, dhi_day)
        axs1[1].set_title('DHI (W/m²)')
        for dni_day in x_plot[:, :, 2]:
            axs1[2].plot(time, dni_day)
        axs1[2].set_title('DNI (W/m²)')
        plt.show()

        # # Subplots for normalised clustering data
        # fig1, axs1 = plt.subplots(3)
        # fig1.suptitle('Clustering data sets')
        # for cons_day in whitened[:, :, 0]:
        #     axs1[0].plot(time, cons_day)
        # axs1[0].set_title('Normalised consumption profiles')
        # for dhi_day in whitened[:, :, 1]:
        #     axs1[1].plot(time, dhi_day)
        # axs1[1].set_title('Normalised DHI')
        # for dni_day in whitened[:, :, 2]:
        #     axs1[2].plot(time, dni_day)
        # axs1[2].set_title('Normalised DNI')
        # plt.show()
        #
        # # subplots for centroids and resulting days from the clustering
        # fig3, axs3 = plt.subplots(3)
        # fig3.suptitle('Clustering normalised results')
        #
        # # Creating lists of values for the centroids:
        # for centroid in centroids:
        #     centro_cons = []
        #     centro_dhi = []
        #     centro_dni = []
        #     for h in range(0, 24):
        #         centro_cons.append(centroid[3*h])
        #         centro_dhi.append(centroid[3*h+1])
        #         centro_dni.append(centroid[3*h+2])
        #
        #     axs3[0].plot(time, centro_cons)
        #     axs3[1].plot(time, centro_dhi)
        #     axs3[2].plot(time, centro_dni)
        # axs3[0].set_title('Centroid consumption profiles')
        # axs3[1].set_title('Centroid DHI')
        # axs3[2].set_title('Centroid DNI')
        # plt.show()

        fig3, axs3 = plt.subplots(3)
        fig3.suptitle('Clustering results')
        for day in closest:
            centro_day_cons = []
            centro_day_dhi = []
            centro_day_dni = []
            for h in range(0, 24):
                centro_day_cons.append(df.Consumption[day*24+h])
                centro_day_dhi.append(df.DHI[day*24+h])
                centro_day_dni.append(df.DNI[day*24+h])
            axs3[0].plot(time, centro_day_cons)
            axs3[1].plot(time, centro_day_dhi)
            axs3[2].plot(time, centro_day_dni)
        axs3[0].set_title('Centroid closest day consumption profiles (W)')
        axs3[1].set_title('Centroid closest day DHI (W/m²)')
        axs3[2].set_title('Centroid closest day DNI (W/m²)')
        plt.show()

        return np.bincount(idx), closest

#
# def load_clustering_data(file_name, typical_day_nb):
#     irradiance, consumption = dm.read_data(file_name='../data/'+file_name)
#
#     day_weight, day_indices = clustering(file_name=file_name,
#                                          centroids_nb=typical_day_nb,
#                                          print_results=False)
#
#     cons_list = []
#     irr_list = []
#     for i in day_indices:
#         cons_list.append([consumption[hour] for hour in range(24*i,
#                                                            24*(i+1))])
#         irr_list.append([irradiance[hour] for hour in range(24*i,
#                                                            24*(i+1))])
#     return cons_list, irr_list


if __name__ == "__main__":
    # feature_selection()
    clustering('irradiance_and_consumption_data.csv', 3,
                   print_results=True)
    # c, i = load_clustering_data('irradiance_and_consumption_data.csv', 3)
    # print(c)
    # print(i)
