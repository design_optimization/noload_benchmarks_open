#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

import pandas as pd
from datetime import datetime, timedelta
from influxdb import DataFrameClient
import numpy as np
from json import loads
from urllib3 import PoolManager, ProxyManager
import socket


def read_data(file_name):
    df = pd.read_csv(file_name, sep=';', decimal=".")
    df.TIMESTAMP = df.TIMESTAMP.apply(pd.to_datetime, '%Y-%m-%d %H')
    # :%M:%S')
    df.set_index(df.TIMESTAMP, inplace=True, drop=True)
    irradiance = pd.DataFrame({
        'DHI': df['DHI'],
        'DNI': df['DNI'],
        # 'DHI': df['Ray_Diffus_RSR2_Avg'],
        # 'DNI': df['Ray_Direct_RSR2_Avg'],
        # 'GHI': df['Ray_Global_RSR2_Avg'],
        # 'WindSpeed': df['WSpd_Avg'],
        # 'AirTemp': df['AirTemp_Avg']
    }, index=df.index)
    consumption = pd.DataFrame({
        'consumption': df['Consumption'],
    }, index=df.index)
    # les données de la météo sont en heure solaire, il faut les laisser !!
    return irradiance.fillna(0), consumption.fillna(0)


def load_irradiance(file_name):
    irradiance, consumption = read_data(file_name='../data/' + file_name)
    return irradiance


def load_consumption(file_name):
    irradiance, consumption = read_data(file_name='../data/' + file_name)
    return consumption.values[:, 0]


def load_irradiance_index(file_name, index_list):
    irradiance, consumption = read_data(file_name=file_name)
    irr_index_list = []
    for index in index_list:
        irr_index_list.append(irradiance.iloc[24 * index:24 * (index + 1)])
    return irr_index_list


def load_consumption_index(file_name, index_list):
    irradiance, consumption = read_data(file_name=file_name)
    cons_index_list = []
    for index in index_list:
        consumption_index = [consumption.values[:, 0][i] for i in \
                                      range(24 * index, 24 * (index + 1))]
        cons_index_list.append(np.array(consumption_index))
    return cons_index_list


def loadSummer():
    return read_data(file_name='../data/summer.csv')


def loadWinter():
    return read_data(file_name='../data/winter.csv')


def getProfilCoefENEDIS(filename):
    df = pd.read_csv(filename, sep=';', decimal=".", parse_dates=True,
                     index_col=0)
    # df.HORODATE = pd.to_datetime(df.HORODATE, utc=False)#.tz_
    # convert(None).tz_localize("Europe/Paris")
    print(df.head())
    df = df.resample('1H').mean()
    print(df.head())
    df.to_csv('resampled_1h_' + filename)  #


def extract_csv_from_mhi(start=datetime(year=2019, month=1, day=1),
                         end=datetime(year=2019, month=12, day=31),
                         timestamp="h"):
    host = 'mhi-srv.g2elab.grenoble-inp.fr'
    port = 8086
    user = 'public'
    password = 'openData4U'
    database_name = 'appli_greener'
    #    database_name = 'pv'
    # epoch = timestamp

    try:
        # making client on influxDB server
        client = DataFrameClient(host, port, user, password, database_name)

        # database = "consumption_v2"
        # database = "nebulosite"
        # database = "nebulosite_v1"
        # database = "sun_v1"
        database = "dashboard"

        FORMAT_DATETIME = "%Y-%m-%d"

        qr_mesuree = client.query(query=
        "SELECT sun_diffus, sun_direct FROM {} WHERE "
        "\"time\">=\'{}\' AND \"time\"<\'{}\'".format(
            database,
            start.strftime(FORMAT_DATETIME),
            end.strftime(FORMAT_DATETIME)))  #, epoch=epoch)
        # IF the data exists;
        if bool(qr_mesuree):
            # pick the dataframe from the dictionary
            # df_mesuree = qr_mesuree["consumption_v2"]
            # df_mesuree = qr_mesuree["nebulosite_v1"]
            df_mesuree = qr_mesuree["dashboard"]
            # export to CSV (separator : coma)
            # df_mesuree.to_csv("conso-GreEn-ER.csv")
            irr_file_name="irradiance_data.csv"
            df_mesuree.to_csv("data/"+irr_file_name, ';')
            print("File {} generated".format(irr_file_name))
    except (TimeoutError, ConnectionError):
        print("Timeout or Connection error: the data server might not be "
              "accessible. If you are running this script on mybinder, "
              "this request cannot be run: please use another service or a "
              "local version of the Jupyter Notebook. Other queries working "
              "on mybinder are currently being developed, with http "
              "requests, authorized by mybinder")


def get_data(url: str):
    """
    Getting data from an API with http request
    :param url: url of the API hosting the data to get
    :return: data_sun
    """
    http = PoolManager()
    response = http.request('GET', url)
    data_result = loads(response.data)

    return data_result


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]


# if __name__ == '__main__':
#     print(read_data('03_01_2019.csv'))

# url_api_env = "http://mhi-srv.g2elab.grenoble-inp.fr/API/environment"
# df = get_data(url_api_env)
