#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from noload.optimization.optimProblem import Spec, OptimProblem
try:
    import pv_battery_coverage_gwp.data.data_management as dm
    import pv_battery_coverage_gwp.models.model as model
    import pv_battery_coverage_gwp.models.solarPV as solarPV
except ModuleNotFoundError:
    import data.data_management as dm
    import models.model as model
    import models.solarPV as solarPV

import numpy as np
import matplotlib.pyplot as plt


class OptimDefinition:
    def __init__(self, objectives):
        # Typical days number choice
        self.typical_day_nb = 3
        #TODO : getting data directly from clustering
        # cons, irr = dm.read_data('../data/irradiance_and_consumption_data.csv')
        # # typical_day_weight, typical_day_indice = \
        # #     clustering('../data/irradiance_and_consumption_data.csv', 3,
        # #                print_results=False)
        # # irr, cons = dm.read_data('../data/irradiance_and_consumption_data.csv')
        # typical_day_weight = [180, 50, 135]
        # typical_day_indice = [240, 3, 330]
        #
        # # importing consumption_data
        # cons_list = []
        # irr_list = []
        # for i in typical_day_indice:
        #     cons_list.append(
        #         [cons.values[:, 0][hour] for hour in range(24 * i, 24 *
        #                                                    (i + 1))])
        #     irr_list.append(irr.iloc[24 * i:24 * (i + 1),:])

        # creating parameters required for the model computation :
        typical_day_weight = [50, 180, 135]
        typical_day_indice = [3, 240, 330]

        # importing consumption_data
        cons_list = dm.load_consumption_index(
            '../data/irradiance_and_consumption_data.csv', typical_day_indice)
        irr_list = dm.load_irradiance_index(
            '../data/irradiance_and_consumption_data.csv', typical_day_indice)

        self.irradiance_list = irr_list
        self.consumption_list = cons_list
        inputs = {'irradiance_list': self.irradiance_list,
                  'consumption_list': self.consumption_list,
                  'typical_day_nb': self.typical_day_nb,
                  'typical_day_weight': typical_day_weight}

        # defining optimization variables
        x_names = ['surface', 'tilt', 'azimuth', 'e_sto_1', 'e_sto_2',
                   'e_sto_3']
        # defining optimisation bounds
        pv_area_min = 1  # PV area above 1 m²
        pv_area_max = 40  # PV area below 21 m²
        e_sto_bounds = []
        e_sto_max = 16000  # capacity below 10 kWh
        # for day in range(0, typical_day_nb):
        # e_sto_bounds.append([0, e_sto_max])
        # e_sto_0.append(e_sto_0_value)
        # e_sto_0 = np.array(e_sto_0)
        # e_sto_bounds = np.array(e_sto_bounds)
        # e_sto_bounds = np.vstack((e_sto_0*0, e_sto_0*20)).T

        bounds = [[pv_area_min, pv_area_max], [0, 90], [-80, 80], [0,
                                                                   e_sto_max],
                  [0, e_sto_max], [0, e_sto_max]]

        # Exports and imports above 0.
        ineq_bounds = [[0, None], [0, None], [0, None], [0, None], [0, None],
                       [0, None]]

        # initial values
        e_sto_0_value = 0  # initial capacity value
        pv_area_0 = 25
        tilt_0 = 45
        azimuth_0 = 0
        x0 = [pv_area_0, tilt_0, azimuth_0, e_sto_0_value, e_sto_0_value,
              e_sto_0_value]

        # creating optimization specifications
        self.spec = Spec(variables=x_names, bounds=bounds, xinit=x0,
                         objectives=objectives,
                         ineq_cstr=['import_0', 'import_1',
                                    'import_2', 'export_0',
                                    'export_1', 'export_2'],
                         ineq_cstr_bnd=ineq_bounds)

        # run the optimisation
        self.optim = OptimProblem(model=model.objectives_and_constraints,
                                  specifications=self.spec, parameters=inputs)

    def plot(self):
        # Plot PV / Consumption
        [surface, tilt, azimuth, e_sto_1, e_sto_2, e_sto_3] = \
            self.optim.wrapper.solution()
        # for (irr, cons) in zip(self.irradiance_list, self.consumption_list):
        #     self.plot_result(surface, tilt, azimuth, irr, cons)
        plot_results(surface, tilt, azimuth, self.irradiance_list,
                          self.consumption_list, self.typical_day_nb)


def plot_result(surface, tilt, azimuth, irradiance, consumption):
        solar = solarPV.PVSolar(irradiance.index.to_series())
        DNI = np.array(irradiance['DNI'])
        DHI = np.array(irradiance['DHI'])
        power = solar.pv_power(tilt, azimuth, surface, DNI, DHI)
        plt.plot(power)
        plt.plot(consumption)
        plt.show()


def plot_results(surface, tilt, azimuth, irradiance_list,
                     consumption_list, typical_day_nb):
    fig, axs = plt.subplots(3)
    fig.suptitle('PV power and consumption for typical days')
    for i in range(0, typical_day_nb):
        solar = solarPV.PVSolar(irradiance_list[i].index.to_series())
        DNI = np.array(irradiance_list[i]['DNI'])
        DHI = np.array(irradiance_list[i]['DHI'])
        power = solar.pv_power(tilt, azimuth, surface, DNI, DHI)
        axs[i].plot(power)
        axs[i].plot(consumption_list[i])
        axs[i].set_xlabel("Time (h)")
        axs[i].set_ylabel("Power (W)")
    plt.show()
