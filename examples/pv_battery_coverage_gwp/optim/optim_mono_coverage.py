#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from pv_battery_coverage_gwp.optim.optim_definition import OptimDefinition
myOpt = OptimDefinition(['non_lcf'])
result = myOpt.optim.run(ftol=1e-9, maxiter=500)
result.printResults()
#result.plotResults()

# It is also possible to iterate by yourself to get results
# outputs = ['sum_supplyCF', 'sum_loadCF', 'win_supplyCF', 'win_loadCF',
# 'nonCoverage']
# for name in outputs:
#    print(name, '  \t =', result.rawResults[name])
for name, value in result.rawResults.items():
    if not (('irradiance' in name) or ('consumption' in name)):
        print(name, '  \t =', value)

# Plot PV / Consumption
myOpt.plot()

