#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0
from pv_battery_coverage_gwp.optim.optim_definition import OptimDefinition
myOpt = OptimDefinition(['biobjectif'])

cMax = 0
myOpt.spec.bounds[3]=[0, cMax]
myOpt.spec.bounds[4]=[0, cMax]

result = myOpt.optim.run(ftol=1e-10, maxiter=200)
result.printResults()
#result.plotResults()

#It is also possible to iterate by yourself to get results
#outputs = ['sum_supplyCF', 'sum_loadCF', 'win_supplyCF', 'win_loadCF', 'nonCoverage']
#for name in outputs:
#    print(name, '  \t =', result.rawResults[name])
for name, value in result.rawResults.items():
    if not (('irradiances' in name) or ('consumption' in name)):
        print(name, '  \t =', value)

#Plot PV / Consumption
myOpt.plot()

