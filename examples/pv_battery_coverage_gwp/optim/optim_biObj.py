#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from pv_battery_coverage_gwp.optim.optim_definition import OptimDefinition

myOpt = OptimDefinition(['non_coverage', 'gwp'])
result = myOpt.optim.run(ftol=1e-16, maxiter=400,  nbParetoPts=8)

result.printResults()
import noload.gui.plotPareto as pp

# TODO intégrer ça dans le wrapper
# TODO ajouter les contraintes sur le pareto

# convergence can be tough, depending on the tolerance (ftol<1e-8) and the
# number of iteration (maxiter>200). Pareto points can be badly gathered,
# you can avoid joining them with : joinDots=False

pp.plot([result.resultsHandler], ['Non-coverage (pu)', 'Global Warming '
                                                      'Potential (kgCO2eq)'],
        ['Pareto'], joinDots=False)
pp.plot([result.resultsHandler], ['Non-coverage (pu)', 'Global Warming '
                                                      'Potential (kgCO2eq)'],
        ['Pareto'])

result.plotResults()

for name, value in result.rawResults.items():
    if not (('irradiances' in name) or ('consumption' in name)):
        print(name, '  \t =', value)
