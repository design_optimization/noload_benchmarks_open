#!/usr/bin/env python
# coding: utf-8

# SPDX-FileCopyrightText: 2020 G2Elab / MAGE
#
# SPDX-License-Identifier: Apache-2.0

from pv_battery_coverage_gwp.optim.optim_definition import OptimDefinition
myOpt = OptimDefinition(['non_lcf', 'gwp'])
nb_pareto = 6
e_sto_max = 16000
pv_area_min = 20
pv_area_max = 50  # PV area below 60 m²
myOpt.spec.bounds[0] = [pv_area_min, pv_area_max]
myOpt.spec.bounds[3] = [0, e_sto_max]
myOpt.spec.bounds[4] = [0, e_sto_max]
myOpt.spec.bounds[5] = [0, e_sto_max]
result = myOpt.optim.run(ftol=1e-11, maxiter=500,  nbParetoPts=nb_pareto)

result.printResults()
import noload.gui.plotPareto as pp
# TODO intégrer ça dans le wrapper
# TODO ajouter les contraintes sur le pareto

# convergence can be tough, depending on the tolerance (ftol<1e-8) and the
# number of iteration (maxiter>200). Pareto points can be badly gathered,
# do not join them : joinDots=False

pp.plot([result.resultsHandler], ['Non_coverage (pu)', 'Global Warming '
                                                      'Potential (kgCO2eq)'],
        ['Pareto'], nb_annotation=nb_pareto, joinDots=False)
# pp.plot(iter=[result.resultsHandler], xyLabels=['Non_coverage (pu)',
#                                                 'Global Warming Potential '
#                                                 '[kgCO2eq]'],
#         legend=['Pareto'], nb_annotation=4)

result.plotResults()

for name, value in result.rawResults.items():
    if not (('irradiance' in name) or ('consumption' in name)):
        print(name, '  \t =', value)

# IndexError: list assignment index out of range

# pareto_in = []*nb_pareto
# pareto_out = []*nb_pareto
# for i in range(nb_pareto):
#     pareto_in[i], pareto_out[i] = result.getIteration(i+1)
# print('inp=', pareto_in)
# print('out=', pareto_out)
