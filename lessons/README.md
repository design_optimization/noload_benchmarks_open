# Lessons
PDF files used as teaching materials
1. French lesson for architects students : enseignement auprès d’étudiants architectes de l’École Nationale Supérieure d’Architecture de Grenoble (ENSAG). L’objectif de ce cours est triple : une introduction aux enjeux énergétiques, à la modélisation énergétique avec les avantages de la science ouverte, et une mise en application de ces principes. Le notebook dimensionnnement_PV_ENSAG est utilisé dans ce cadre.
